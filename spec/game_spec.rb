require 'rspec'
require 'game'

describe Game do
  subject(:game) {Game.new([])}
  
  it "game has 5 players" do
    game.create_players(5)
    game.players.length.should == 5
  end
  
  it "game has been initialized" do
    game.players.should be_empty
  end
  
  describe 'get_deck' do
    it "gets the deck" do
      # Make sure it's actually shuffled
      d = game.get_deck
      d.should eq game.deck
    end
  end
  
  describe 'draw_card' do
    before do
      game.get_deck
    end
    it 'grabs a card at random' do
      game.draw_cards.should_not be_empty
    end
    
    it 'grabs 3 cards at random' do
      game.draw_cards(3).length.should eq 3
    end
    
    it 'deletes card from deck after drawing' do
      len_before = game.deck.length
      game.draw_cards(3)
      expect(game.deck.length).to_not eq len_before
    end
    
  end
  
  describe 'create_players' do
    it 'creates a default of 4 players' do
      game.create_players
      expect(game.players.length).to eq 4
    end
    
    it 'gives players card upon creating them' do
      game.create_players
      expect(game.players[0].hand.length).to_not eq 0
    end
  end
  
end