require "rspec"
require "card"

describe Card do
  it "initializes a king of clubs" do
    card = Card.new(13,:club)
    expect(card.type).to eq 13 
  end
  
  it "initializes a 3 of spades" do
    card = Card.new(3, :spades)
    expect(card.suit).to eq :spades 
  end
  
end