require "rspec"
require "player"

describe Player do
  subject(:player) {Player.new}
  it "has a pot"
  it "prompts to discard"
  it "prompts to fold, see, or raise"
end