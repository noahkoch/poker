require "rspec"
require "hand"

describe Hand do
  describe "pairs" do
    subject(:hand){Hand.new}
    it "recognizes high card" do
      hand.pairs([13,14, 1, 2, 3]).should eq 14
    end
    it "recognizes one-pair" do
      hand.pairs([13, 3, 1, 2, 3]).should eq [3]
    end
    it "recognizes two-pair" do 
      hand.pairs([13, 4, 2, 2, 4]).should eq [2,4]
    end
    it "recognizes three-of-a-kind" do
      hand.pairs([13, 2, 4, 4, 4]).should eq [:three,4]
    end
    it "recognizes four-of a kind" do
      hand.pairs([13, 4, 4, 4, 4]).should eq [:four,4]
    end  
    it "recognizes full house" do
      hand.pairs([13, 4, 4, 13, 4]).should eq [:full,4,13]
    end  
  end
  
  describe "suits" do
    subject(:hand){Hand.new}
    it "recognizes flush" do
     hand.suits([[13,:spade], [4,:spade], [4,:spade], [13,:spade], [4,:spade]]).should eq [:flush,13]
    end
    it "recognizes no flush" do
      hand.suits([[13,:spade], [4,:spade], [4,:hearts], [13,:spade], [4,:spade]]).should be_nil
    end
    it "recognizes straight flush"  do
      hand.suits([[5,:spade], [4,:spade], [6,:spade], [8,:spade], [7,:spade]]).should eq [:straightflush,8]
    end
    it "recognizes royal flush" do
      hand.suits([[14,:spade], [11,:spade], [10,:spade], [13,:spade], [12,:spade]]).should eq [:royalflush]
    end
  end
  
  describe "straights" do
    subject(:hand){Hand.new}
    it "recognizes straight" do
      hand.straights([[2,:spade], [3,:spade], [5,:spade], [4,:spade], [6,:spade]]).should eq [:straight,6]
    end
  end
  
  it "recognizes royal flush"
  it "orders precedence of hands"
end
