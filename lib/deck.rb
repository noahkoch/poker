class Deck
  attr_accessor :deck
  def initialize
    @deck = []
    populate
  end
  
  def populate
    types = [14,13,12,11,10,9,8,7,6,5,4,3,2]
    suits = [:hearts,:spades,:clubs,:diamonds]
    types.each{|type| suits.each{|suit| deck << Card.new(type,suit) }}
  end
  
end