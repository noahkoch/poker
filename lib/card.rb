class Card
  attr_accessor :suit, :type
  def initialize(type, suit)
    @suit = suit
    @type = type
  end
end