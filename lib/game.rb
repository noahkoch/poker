class Game
  attr_accessor :players, :deck
  def initialize(players)
    @players = players
    @deck = []
  end
  
  def draw_cards(cards = 1)
    output = []
    cards.times do
      card = @deck.sample
      output << card
      deck.delete(card)
    end
    output
  end
  
  def get_deck
    d = Deck.new
    @deck = d.deck
  end
  
  def create_players(n = 4)
    n.times do
      player = Player.new
      player.get_cards(draw_cards(5))
      players << player
    end
  end
end