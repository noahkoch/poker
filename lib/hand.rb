class Hand
  TYPES = [14,13,12,11,10,9,8,7,6,5,4,3,2]
  
  def pairs(hand)
    high = nil
    pairs_in_hand = []
    # high card
    high = get_high_card(hand)
    # find one and two pair
    hand.each_with_index{ |card, index| pairs_in_hand << card if (hand[0...index].include? card)}
    # returns high card
    if pairs_in_hand.empty?
      high unless high == 0
    # returns three/four of a kind
    else
      parse_pairs(pairs_in_hand)
    end
  end
  
  def suits(hand)
    # flush
    flat_hand = hand.flatten
    if (suit_count = flat_hand.count(hand[0][1])) == 5
      if straights(hand) != nil
        return get_high_card(flat_hand) == 14 ? [:royalflush] : [:straightflush, get_high_card(flat_hand)]
      else
        return [:flush, get_high_card(flat_hand)]
      end
    end
  end
  
  def straights(hand)
    hand = hand.sort.flatten.map{|c| c if (TYPES.include? c)}.compact.reverse
    first, second = TYPES.index(hand[0]), TYPES.index(hand[-1])
    if ((second - first).abs == 4)
      return [:straight,hand[0]]
    end
    nil
  end
  
  def parse_pairs(pairs)
    # if the unique array is different from the regular array, pairs exist. 
    # This helps us find how many. 
    unique = pairs.uniq
    if unique.length < pairs.length
      case (pairs.length - unique.length)
      when 1 
      (unique.count == 2) ? [:full,unique[0],unique[1]] : [:three,unique[0]]
      when 2
        return [:four,unique[0]]
      end  
    # retruns one/two pair
    else
      return pairs
    end
  end
  
  def get_high_card(cards)
    high = nil
    TYPES.each{ |card| high = card if (cards.include?(card) && high == nil) }
    return high
  end
  
end